﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ParallelTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var Proc = Process.GetCurrentProcess();
            long AffinityMask = (long)Proc.ProcessorAffinity;
            AffinityMask &= 0x000E; // use only any of the first 4 available processors
            Proc.ProcessorAffinity = (IntPtr)AffinityMask;
            Parallel.Invoke(() => Slow(9000000));//, () => Slow(100000000));            
           
            //var Thread = Proc.Threads[0];
            //AffinityMask = 0x0002; // use only the second processor, despite availability
            //Thread.ProcessorAffinity = (IntPtr)AffinityMask;
        }

        public static void Slow(int i)
        {
            long nthPrime = FindPrimeNumber(i); //set higher value for more time
            Console.WriteLine(nthPrime);
        }

        public static long FindPrimeNumber(int n)
        {
            int count = 0;
            long a = 2;
            while (count < n)
            {
                long b = 2;
                int prime = 1;// to check if found a prime
                while (b * b <= a)
                {
                    if (a % b == 0)
                    {
                        prime = 0;
                        break;
                    }
                    b++;
                }
                if (prime > 0)
                {
                    count++;
                }
                a++;
            }
            return (--a);
        }
    }
}