﻿using System;

namespace Id3v2
{
    /// <summary>
    /// Class for handling messages to the console
    /// </summary>
    public static class MessageHandler
    {
        /// <summary>
        /// Displays error message.
        /// </summary>
        /// <param name="errorMessage"></param>
        public static void DisplayErrorMessage(string errorMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\n{errorMessage}\n");
            Console.ResetColor();
        }
    }
}