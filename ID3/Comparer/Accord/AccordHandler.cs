﻿using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Math;
using Accord.Statistics.Filters;
using ID3.Services.Extensions;
using Id3v2.Services.Contracts;
using Id3v2.Services.Implementations;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ID3Comparer.Accord
{
    public interface IAccordHandler : IService
    {
        List<string> Handle(DataTable trainingData, List<Dictionary<string, string>> testData);
    }

    public class AccordHandler : IAccordHandler
    {
        private readonly IDifferentAttributesGetter _differentAttributesGetter;

        public AccordHandler(IDifferentAttributesGetter differentAttributesGetter)
        {
            _differentAttributesGetter = differentAttributesGetter;
        }

        public List<string> Handle(DataTable trainingData, List<Dictionary<string, string>> testData)
        {
            var codebook = new Codification(trainingData);

            DataTable symbols = codebook.Apply(trainingData);
            string[] columnsNames = trainingData.GetColumnsNames();
            string[] notDecisionColumnsNames = columnsNames.Take(columnsNames.Length - 1).ToArray();

            int[][] inputs = symbols.ToJagged<int>(notDecisionColumnsNames);
            int[] outputs = symbols.ToArray<int>(columnsNames.Last());

            DecisionVariable[] variables = GetVariables(trainingData);

            var id3 = new ID3Learning(variables);

            var tree = id3.Learn(inputs, outputs);
          return  CalculateResult(testData, codebook, columnsNames, tree);
        }

        private List<string> CalculateResult(List<Dictionary<string, string>> testData, Codification codebook, string[] columnsNames, DecisionTree tree)
        {
            var results = new List<string>();
            foreach (var item in testData)
            {
                int[] query = PrepareQuery(codebook, item);

                int predicted = tree.Decide(query);

                string answer = codebook.Revert(columnsNames.Last(), predicted);
                results.Add(answer);
            }

            return results;
        }

        private int[] PrepareQuery(Codification codebook, Dictionary<string, string> testData)
        {
            var values = new List<List<string>>();

            for (int i = 0; i < testData.Count; i++)
            {
                values.Add(new List<string>() { testData.ElementAt(i).Key, testData.ElementAt(i).Value });
            }

            var t = values.Select(x => x.ToArray()).ToArray().ToMatrix();
            return codebook.Transform(t);
        }

        private DecisionVariable[] GetVariables(DataTable table)
        {
            var decisionVariables = new List<DecisionVariable>();
            for (int i = 0; i < table.Columns.Count - 1; i++)
            {
                var attributeValuesNames = _differentAttributesGetter.GetDifferentAttributeValuesNames(table, i);
                decisionVariables.Add(new DecisionVariable(table.Columns[i].ColumnName, attributeValuesNames.Count));
            }

            return decisionVariables.ToArray();
        }
    }
}
