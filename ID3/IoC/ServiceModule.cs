﻿using Autofac;
using Id3v2.Services.Contracts;
using Id3v2.Services.Implementations;
using System.Reflection;

namespace Id3v2.IoC
{
    /// <summary>
    /// Module with services for application calculations
    /// </summary>
    public class ServiceModule : Autofac.Module
    {
        /// <summary>
        /// Loads services dependencies.
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IService>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
            
            builder.RegisterType<FileHandler>()
                .As<IFileHandler>()
                .SingleInstance();
        }
    }
}