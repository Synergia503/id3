﻿using Autofac;

namespace Id3v2.IoC
{
    /// <summary>
    /// Container for other modules
    /// </summary>
    public class ContainerModule : Module
    {
        public ContainerModule()
        {
        }

        /// <summary>
        /// Loads modolues with registers dependencies.
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<ServiceModule>();
        }
    }
}