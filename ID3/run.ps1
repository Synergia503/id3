$optionsCount=15
$loopCount=50
$datasetName="nursery"

dotnet build -c Release
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 01-parallel-tree-creating
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 02-parallel-get-different-attributes
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 03-parallel-get-different-attributes-concurrent-dictionary
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 04-parallel-calculate-table-entropy
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 05-parallel-invoke-get-root-node
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout 06-concurrent-getting-result
dotnet build 
for($n=1; $n -le $optionsCount; $n++){
	for($i=1; $i -le $loopCount; $i++){
		dotnet bin\Release\netcoreapp2.1\ID3.dll "$i" $datasetName "csv" ";" "$n"
	}
}

git checkout master