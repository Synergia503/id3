﻿using System;

namespace Id3v2.Domain
{
    /// <summary>
    /// Tree model
    /// </summary>
    public class Tree
    {
        public TreeNode Root { get; private set; }

        /// <summary>
        /// Sets tree's root.
        /// </summary>
        /// <param name="node"></param>
        public void SetRoot(TreeNode node)
        {
            Root = node ?? throw new NullReferenceException("Root must not be null.");
        }
    }
}