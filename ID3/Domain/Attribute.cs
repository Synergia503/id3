﻿using System;
using System.Collections.Generic;

namespace Id3v2.Domain
{
    /// <summary>
    /// Attribute model.
    /// </summary>
    public class Attribute
    {
        /// <summary>
        /// Initializes attribute instance.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="differentAttributeValuesNames"></param>
        public Attribute(string name, List<string> differentAttributeValuesNames)
        {
            Name = name;
            DifferentAttributeValuesNames = differentAttributeValuesNames;
        }

        public string Name { get; }
        public double InformationGain { get; private set; }
        public List<string> DifferentAttributeValuesNames { get; }

        /// <summary>
        /// Sets information gain.
        /// </summary>
        /// <param name="informationGain"></param>
        public void SetInformationGain(double informationGain)
        {
            if (informationGain < 0)
            {
                throw new Exception("InformationGain must not be below 0.");
            }

            InformationGain = informationGain;
        }

        public static Attribute operator >(Attribute a, Attribute b)
        {
            return a.InformationGain > b.InformationGain ? a : b;
        }

        public static Attribute operator <(Attribute a, Attribute b)
        {
            return a.InformationGain < b.InformationGain ? a : b;
        }
    }
}