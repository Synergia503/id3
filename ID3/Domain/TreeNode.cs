﻿using System;
using System.Collections.Generic;

namespace Id3v2.Domain
{
    /// <summary>
    /// Tree node model
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// Initializes tree node instance as branch.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="columnIndex"></param>
        /// <param name="attribute"></param>
        /// <param name="branchName"></param>
        public TreeNode(string name, int columnIndex, Attribute attribute, string branchName)
        {
            Name = name;
            ChildNodes = new List<TreeNode>();
            ColumnIndex = columnIndex;
            NodeAttribute = attribute;
            BranchName = branchName;
        }

        /// <summary>
        /// Initializes tree node instance as leaf.
        /// </summary>
        /// <param name="endValue"></param>
        /// <param name="branchName"></param>
        public TreeNode(string endValue, string branchName)
        {
            SetEndValue(endValue);
            IsLeaf = true;
            SetBranchName(branchName);
        }

        public string Name { get; private set; }
        public string EndValue { get; private set; }
        public List<TreeNode> ChildNodes { get; private set; }
        public Attribute NodeAttribute { get; private set; }
        public string BranchName { get; private set; }
        public int ColumnIndex { get; private set; }
        public bool IsLeaf { get; private set; }

        /// <summary>
        /// Sets name of tree node. 
        /// </summary>
        /// <param name="name"></param>
        public void SetName(string name)
        {
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new Exception("Name must not be empty.");
        }

        /// <summary>
        /// Sets end value of leaf.
        /// </summary>
        /// <param name="endValue"></param>
        public void SetEndValue(string endValue)
        {
            EndValue = !string.IsNullOrWhiteSpace(endValue) ? endValue : throw new Exception("End value must not be empty.");
        }

        /// <summary>
        /// Sets childs nodes for node.
        /// </summary>
        /// <param name="childNodes"></param>
        public void SetChildNodes(List<TreeNode> childNodes)
        {
            ChildNodes = childNodes ?? throw new NullReferenceException("Child nodes must not be null.");
        }

        /// <summary>
        /// Sets attribute for node.
        /// </summary>
        /// <param name="nodeAttribute"></param>
        public void SetNodeAttribute(Attribute nodeAttribute)
        {
            NodeAttribute = nodeAttribute ?? throw new NullReferenceException("Node attribute nodes must not be null.");
        }

        /// <summary>
        /// Sets branch name for node.
        /// </summary>
        /// <param name="branchName"></param>
        public void SetBranchName(string branchName)
        {
            BranchName = !string.IsNullOrWhiteSpace(branchName) ? branchName : throw new Exception("Branch name must not be empty.");
        }

        /// <summary>
        /// Sets column index for node based on training data.
        /// </summary>
        /// <param name="index"></param>
        public void SetColumnIndex(int index)
        {
            ColumnIndex = (index >= 0) ? index : throw new Exception("Column index must not be below 0.");
        }
    }
}