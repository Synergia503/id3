﻿using Id3v2.Services.Contracts;
using System;
using System.Collections.Generic;
using static System.Console;

namespace ID3.Services.Measurements
{
    public interface IResultPrinter : IService
    {
        void PrintSpeedup(TimeSpan baseTime, Dictionary<string, TimeSpan> processingTimes);
    }

    public class ResultPrinter : IResultPrinter
    {
        public void PrintSpeedup(TimeSpan baseTime, Dictionary<string, TimeSpan> processingTimes)
        {
            Write("Base time".PadRight(50));
            WriteLine(baseTime);
            Write("Calculations kind".PadRight(50));
            Write("Time".PadRight(40));
            WriteLine("Acceleration");

            foreach (KeyValuePair<string, TimeSpan> actionWithMeasure in processingTimes)
            {
                double speedupScale = baseTime.TotalMilliseconds / actionWithMeasure.Value.TotalMilliseconds;
                Write(actionWithMeasure.Key.PadRight(50));
                Write(actionWithMeasure.Value.ToString().PadRight(40));
                WriteLine(speedupScale);
            }
        }
    }
}