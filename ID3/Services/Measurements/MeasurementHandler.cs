﻿using Id3v2.Services.Contracts;
using System;
using System.Diagnostics;

namespace Id3v2.Services.Measurements
{
    public interface IMeasurementHandler : IService
    {
        TimeSpan Measure(Action action);
    }

    public class MeasurementHandler : IMeasurementHandler
    {
        public TimeSpan Measure(Action action)
        {
            var watch = new Stopwatch();
            watch.Start();
            action();
            watch.Stop();
            return watch.Elapsed;
        }
    }
}