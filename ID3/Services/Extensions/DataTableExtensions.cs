﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ID3.Services.Extensions
{
    public static class DataTableExtensions
    {
        public static string[] GetColumnsNames(this DataTable data)
        {
            var names = new List<string>() { };
            for (int i = 0; i < data.Columns.Count; i++)
            {
                names.Add(data.Columns[i].ColumnName.ToString());
            }

            return names.ToArray();
        }
    }
}