﻿using Id3v2.Services.Contracts;
using System.Collections.Generic;
using System.Data;

namespace Id3v2.Services.Implementations
{
    public interface ITestDataProcessor : IService
    {
        /// <summary>
        /// Process test query from file.
        /// </summary>
        /// <param name="testData"></param>
        /// <returns></returns>
        List<Dictionary<string, string>> ProcessQueriesFromDataTable(DataTable testData);
    }

    public class TestDataProcessor : ITestDataProcessor
    {
        /// <summary>
        /// Process test query from file.
        /// </summary>
        /// <param name="testData"></param>
        /// <returns></returns>
        public List<Dictionary<string, string>> ProcessQueriesFromDataTable(DataTable testData)
        {
            var queries = new List<Dictionary<string, string>>(testData.Rows.Count);
            for (int i = 0; i < testData.Rows.Count; i++)
            {
                queries.Add(new Dictionary<string, string>());
                DataRow row = testData.Rows[i];

                for (int j = 0; j < row.ItemArray.Length; j++)
                {
                    queries[i].TryAdd(testData.Columns[j].ToString(), row.ItemArray[j].ToString());
                }
            }

            return queries;
        }
    }
}