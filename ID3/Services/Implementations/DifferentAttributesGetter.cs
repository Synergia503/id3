﻿using Id3v2.Services.Contracts;
using System.Collections.Generic;
using System.Data;

namespace Id3v2.Services.Implementations
{
    public interface IDifferentAttributesGetter : IService
    {
        /// <summary>
        /// Extracts different values' names for each attribute.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        List<string> GetDifferentAttributeValuesNames(DataTable data, int columnIndex);
    }

    /// <summary>
    /// Extracts different values' names for each attribute.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="columnIndex"></param>
    /// <returns></returns>
    public class DifferentAttributesGetter : IDifferentAttributesGetter
    {
        public List<string> GetDifferentAttributeValuesNames(DataTable data, int columnIndex)
        {
            var differentAttributeValuesNames = new List<string>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string cell = data.Rows[i][columnIndex].ToString();
                if (!differentAttributeValuesNames.Contains(cell))
                {
                    differentAttributeValuesNames.Add(cell);
                }
            }

            return differentAttributeValuesNames;
        }
    }
}