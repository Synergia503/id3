﻿using Id3v2.Domain;
using Id3v2.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Attribute = Id3v2.Domain.Attribute;

namespace Id3v2.Services.Implementations
{
    public interface IRootNodeFinder : IService
    {
        /// <summary>
        /// Gets root node for tree.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        TreeNode GetRootNode(DataTable data, string branchName);
    }

    public class RootNodeFinder : IRootNodeFinder
    {
        private readonly IDifferentAttributesGetter _differentAttributesGetter;
        private readonly ITreeCalculator _treeCalculator;

        public RootNodeFinder(
            IDifferentAttributesGetter differentAttributesGetter,
            ITreeCalculator treeCalculator)
        {
            _differentAttributesGetter = differentAttributesGetter;
            _treeCalculator = treeCalculator;
        }

        /// <summary>
        /// Gets root node for tree.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public TreeNode GetRootNode(DataTable data, string branchName)
        {
            var attributes = new List<Attribute>();
            for (int i = 0; i < data.Columns.Count; i++)
            {
                List<string> differentValuesForColumn = _differentAttributesGetter.GetDifferentAttributeValuesNames(data, i);
                attributes.Add(new Attribute(data.Columns[i].ToString(), differentValuesForColumn));
            }

            decimal entropy = _treeCalculator.CalculateEntropyForTable(data, attributes.Last().DifferentAttributeValuesNames);
            for (int i = 0; i < attributes.Count - 1; i++)
            {
                decimal attrInfo = _treeCalculator.CalculateAttributeInformation(data, i, attributes.Last().DifferentAttributeValuesNames);
                double attributeInformationGain = (double)Math.Round(entropy, 10) - (double)Math.Round(attrInfo, 10);
                attributes[i].SetInformationGain(attributeInformationGain);
            }

            Attribute rootAttribute = attributes.Aggregate((first, second) => first > second);

            int columnIndexOfBestAttribute = attributes.IndexOf(rootAttribute);
            return new TreeNode(rootAttribute.Name, columnIndexOfBestAttribute, rootAttribute, branchName);
        }
    }
}