﻿using Id3v2.Services.Contracts;
using System.Data;

namespace Id3v2.Services.Implementations
{
    public interface IDataTableHandler : IService
    {
        /// <summary>
        /// Creates smaller table for next iterations.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="edge"></param>
        /// <returns></returns>
        DataTable CreateSmallerTable(DataTable data, int columnIndex, string edge);
    }

    public class DataTableHandler : IDataTableHandler
    {
        /// <summary>
        /// Creates smaller table for next iterations.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="edge"></param>
        /// <returns></returns>
        public DataTable CreateSmallerTable(DataTable data, int columnIndex, string potencialEdge)
        {
            DataTable smaller = new DataTable();
            foreach (DataColumn column in data.Columns)
            {
                smaller.Columns.Add(column.ToString());
            }

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (data.Rows[i][columnIndex].ToString().Equals(potencialEdge))
                {
                    string[] newRow = new string[data.Columns.Count];
                    for (int j = 0; j < data.Columns.Count; j++)
                    {
                        newRow[j] = data.Rows[i][j].ToString();
                    }

                    smaller.Rows.Add(newRow);
                }
            }

            smaller.Columns.Remove(smaller.Columns[columnIndex]);

            return smaller;
        }
    }
}