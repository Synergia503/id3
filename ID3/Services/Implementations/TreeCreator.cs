﻿using Id3v2.Domain;
using Id3v2.Services.Contracts;
using System.Data;
using System.Threading.Tasks;

namespace Id3v2.Services.Implementations
{
    public interface ITreeCreator : IService
    {
        /// <summary>
        /// Performs learning based on ID3 algorithm.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        TreeNode Learn(DataTable data, string branchName);
    }

    public class TreeCreator : ITreeCreator
    {
        private readonly IRootNodeFinder _rootNodeFinder;
        private readonly IDataTableHandler _dataTableHandler;
        private readonly ITreeCalculator _treeCalculator;

        public TreeCreator(
            IRootNodeFinder rootNodeFinder,
            IDataTableHandler dataTableHandler,
            ITreeCalculator treeCalculator)
        {
            _rootNodeFinder = rootNodeFinder;
            _dataTableHandler = dataTableHandler;
            _treeCalculator = treeCalculator;
        }

        /// <summary>
        /// Performs learning based on ID3 algorithm.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public TreeNode Learn(DataTable data, string branchName)
        {
            TreeNode root = _rootNodeFinder.GetRootNode(data, branchName);

            foreach (string singleAttribute in root.NodeAttribute.DifferentAttributeValuesNames)
            {
                bool isAttributeLeaf = _treeCalculator.CheckIfNodeIsLeaf(root, data, singleAttribute);

                if (!isAttributeLeaf)
                {
                    DataTable smaller = _dataTableHandler.CreateSmallerTable(data, root.ColumnIndex, singleAttribute);
                    root.ChildNodes.Add(Learn(smaller, singleAttribute));
                }
            }

            return root;
        }
    }
}