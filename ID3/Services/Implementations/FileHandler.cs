﻿using System;
using System.Data;
using System.IO;
using System.Linq;

namespace Id3v2.Services.Implementations
{
    public interface IFileHandler
    {
        /// <summary>
        /// Imports training data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        DataTable ImportTrainingData(string fileName, char delimeter);

        /// <summary>
        /// Imports test data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        DataTable ImportTestData(string fileName, char delimiter);
    }

    public class FileHandler : IFileHandler
    {
        private readonly IDifferentAttributesGetter _differentAttributesGetter;

        public FileHandler(IDifferentAttributesGetter differentAttributesGetter)
        {
            _differentAttributesGetter = differentAttributesGetter;
        }

        /// <summary>
        /// Imports training data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public DataTable ImportTrainingData(string fileName, char delimeter)
        {
            string[] lines = ReadLinesFromFile(fileName);
            return SplitIntoColumnsAndRows(lines, delimeter);
        }

        /// <summary>
        /// Imports test data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public DataTable ImportTestData(string fileName, char delimiter)
        {
            string[] lines = ReadLinesFromFile(fileName);
            return ProcessToTestData(lines, delimiter);
        }

        /// <summary>
        /// Processes lines to datatables
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        private DataTable ProcessToTestData(string[] lines, char delimiter)
        {
            try
            {
                DataTable data = GetColumnsAndRows(lines, delimiter);
                return data;
            }
            catch (Exception ex)
            {
                MessageHandler.DisplayErrorMessage(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Reads lines from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string[] ReadLinesFromFile(string fileName)
        {
            string[] lines = File.ReadAllLines(
                $"D:/datasets/{fileName.Split(new char[] { '.', '-' }, StringSplitOptions.RemoveEmptyEntries)[0]}/{fileName}");
            return lines;
        }

        /// <summary>
        /// Splits lines into columns and rows and makes datatable.
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        private DataTable SplitIntoColumnsAndRows(string[] lines, char delimeter)
        {
            try
            {
                DataTable data = GetColumnsAndRows(lines, delimeter);
                return data;
            }
            catch (Exception ex)
            {
                MessageHandler.DisplayErrorMessage(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets columns and rows from lines.
        /// </summary> 
        /// <param name="lines"></param>
        /// <returns></returns>
        private DataTable GetColumnsAndRows(string[] lines, char delimiter)
        {
            var data = new DataTable();
            string[] columnsNames = lines.First().TrimEnd(delimiter).Split(delimiter);
            CheckCellsInLine(columnsNames);

            foreach (string columnName in columnsNames)
            {
                data.Columns.Add(columnName);
            }

            for (int i = 1; i < lines.Length; i++)
            {
                string[] cells = lines[i].TrimEnd(delimiter).Split(delimiter);
                data.Rows.Add(cells);
                CheckRowLength(data.Columns.Count, cells.Length, i);
            }

            return data;
        }

        /// <summary>
        /// Checks if cell is empty.
        /// </summary>
        /// <param name="cells"></param>
        private void CheckCellsInLine(string[] cells)
        {
            //todo: maybe try check more cells than from single row
            foreach (string cell in cells)
            {
                if (string.IsNullOrWhiteSpace(cell))
                {
                    throw new Exception("Cell must not be empty!");
                }
            }
        }

        /// <summary>
        /// Checks if row is shorter than row with columns' names.
        /// </summary>
        /// <param name="columnsCount"></param>
        /// <param name="cellsInRowCount"></param>
        /// <param name="rowIndex"></param>
        private void CheckRowLength(int columnsCount, int cellsInRowCount, int rowIndex)
        {
            if (columnsCount != cellsInRowCount)
            {
                throw new Exception($"Row no: {rowIndex + 1} is shorter or longer than title row!");
            }
        }
    }
}