﻿using Id3v2.Domain;
using Id3v2.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Id3v2.Services.Implementations
{
    public interface ITreeCalculator : IService
    {
        /// <summary>
        /// Calculates information gain
        /// </summary>
        /// <param name="data"></param>
        /// <param name="attributeColumnIndex"></param>
        /// <returns></returns>
        decimal CalculateAttributeInformation(DataTable data, int attributeColumnIndex, List<string> differentValuesForLastColumn);

        /// <summary>
        /// Calculates entropy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="differentValuesForDecisionColumn"></param>
        /// <returns></returns>
        decimal CalculateEntropyForTable(DataTable data, List<string> differentValuesForDecisionColumn);

        /// <summary>
        /// Counts total number of examples in learning set
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        int CountTotalExamplesInLearningSet(DataTable data, int columnIndex, string value);

        /// <summary>
        /// Counts total number of positive examples in learning set
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        int CountOccurencesOfClassificationValueForColumn(DataTable data, int columnIndex, string value, string valueInLastColumn);

        /// <summary>
        /// Checks if node is leaf
        /// </summary>
        /// <param name="root"></param>
        /// <param name="data"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        bool CheckIfNodeIsLeaf(TreeNode root, DataTable data, string attribute);

        /// <summary>
        /// Calculates results for test data
        /// </summary>
        /// <param name="root"></param>
        /// <param name="testData"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        string CalculateQueryResult(TreeNode root, Dictionary<string, string> testData, string result);
    }

    public class TreeCalculator : ITreeCalculator
    {
        private readonly IDifferentAttributesGetter _differentAttributesGetter;

        public TreeCalculator(IDifferentAttributesGetter differentAttributesGetter)
        {
            _differentAttributesGetter = differentAttributesGetter;
        }

        /// <summary>
        /// Calculates information gain
        /// </summary>
        /// <param name="data"></param>
        /// <param name="attributeColumnIndex"></param>
        /// <returns></returns>
        public decimal CalculateAttributeInformation(DataTable data, int attributeColumnIndex, List<string> differentValuesForLastColumn)
        {
            List<string> valuesForAttribute = _differentAttributesGetter.GetDifferentAttributeValuesNames(data, attributeColumnIndex);
            decimal informationGain = 0;

            foreach (string value in valuesForAttribute)
            {
                decimal innerEntropy = 0;
                int totalExamplesCountInLearningSet = CountTotalExamplesInLearningSet(data, attributeColumnIndex, value);

                foreach (string valueInLastColumn in differentValuesForLastColumn)
                {
                    int positiveValuesInLearningSet = CountOccurencesOfClassificationValueForColumn(data, attributeColumnIndex, value, valueInLastColumn);

                    if (positiveValuesInLearningSet != 0 && (totalExamplesCountInLearningSet - positiveValuesInLearningSet) != 0)
                    {
                        decimal division = positiveValuesInLearningSet / (decimal)totalExamplesCountInLearningSet;
                        innerEntropy += -division * (decimal)Math.Log((double)division, 2);
                    }
                }

                informationGain += (totalExamplesCountInLearningSet / (decimal)data.Rows.Count) * innerEntropy;
            }

            return informationGain;
        }

        /// <summary>
        /// Counts total number of examples in learning set
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public int CountTotalExamplesInLearningSet(DataTable data, int columnIndex, string value)
        {
            int itemsCount = data.AsEnumerable().Count(x => x[columnIndex].ToString().Equals(value));
            return itemsCount;
        }

        /// <summary>
        /// Counts total number of positive examples in learning set
        /// </summary>
        /// <param name="data"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public int CountOccurencesOfClassificationValueForColumn(DataTable data, int columnIndex, string value, string valueInLastColumn)
        {
            int itemsCount = 0;

            for (int i = 0; i < data.Rows.Count; i++)
            {
                if (data.Rows[i][columnIndex].ToString().Equals(value))
                {
                    if (data.Rows[i][data.Columns.Count - 1].ToString().Equals(valueInLastColumn))
                    {
                        itemsCount++;
                    }
                }
            }

            return itemsCount;
        }

        /// <summary>
        /// Calculates entropy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="differentValuesForDecisionColumn"></param>
        /// <returns></returns>
        public decimal CalculateEntropyForTable(DataTable data, List<string> differentValuesForLastColumn)
        {
            decimal entropy = 0;

            foreach (string item in differentValuesForLastColumn)
            {
                int itemsCount = data.AsEnumerable().Count(x => x[data.Columns.Count - 1].ToString().Equals(item));
                decimal division = (decimal)(itemsCount * 1.0) / (data.Rows.Count);
                entropy += -division * (decimal)Math.Log((double)division, 2);
            }

            return entropy;
        }

        /// <summary>
        /// Checks if node is leaf
        /// </summary>
        /// <param name="root"></param>
        /// <param name="data"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool CheckIfNodeIsLeaf(TreeNode root, DataTable data, string attribute)
        {
            List<DataRow> rowsWithValueOfAttribute = data.AsEnumerable()
                .Where(x => x[root.ColumnIndex].ToString().Equals(attribute))
                .ToList();

            IEnumerable<string> classesForAttribute = rowsWithValueOfAttribute.Select(x => x[data.Columns.Count - 1].ToString());
            var isLeaf = classesForAttribute.Distinct().Count() == 1;

            if (isLeaf)
            {
                root.ChildNodes.Add(new TreeNode(rowsWithValueOfAttribute.FirstOrDefault()[data.Columns.Count - 1].ToString(), attribute));
            }

            return isLeaf;
        }

        /// <summary>
        /// Calculates results for test data
        /// </summary>
        /// <param name="root"></param>
        /// <param name="testData"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public string CalculateQueryResult(TreeNode root, Dictionary<string, string> testData, string result)
        {
            result += root.Name + " --> ";
            if (root.IsLeaf)
            {
                result = root.BranchName + " --> " + root.EndValue ?? root.BranchName + " --> " + root.Name;
            }
            else
            {
                foreach (TreeNode childNode in root.ChildNodes)
                {
                    foreach (KeyValuePair<string, string> item in testData)
                    {
                        if (
                            childNode.BranchName.Equals(item.Value, StringComparison.OrdinalIgnoreCase)
                            &&
                            root.Name.ToLower().Equals(item.Key, StringComparison.OrdinalIgnoreCase))
                        {
                            testData.Remove(item.Key);
                            string pass = childNode.BranchName;
                            return $"{result} {CalculateQueryResult(childNode, testData, pass + " --> ")}";
                        }
                    }
                }
            }

            return result;
        }
    }
}