﻿using Id3v2.Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ID3.Services.Implementations
{
    public interface ICalculationWriter : IService
    {
        Task Save(Dictionary<string, TimeSpan> records);
        string PrepareLine(Dictionary<string, TimeSpan> records);
    }

    public class CalculationWriter : ICalculationWriter
    {
        public async Task Save(Dictionary<string, TimeSpan> records)
        {
            string lineToBeWritten = PrepareLine(records);
            using (var writer = new StreamWriter("D:\\file.csv", append: true))
            {
                await writer.WriteLineAsync(lineToBeWritten);
            }
        }

        public string PrepareLine(Dictionary<string, TimeSpan> records)
        {
            string res = string.Empty;
            foreach (KeyValuePair<string, TimeSpan> record in records)
            {
                res += $"{record.Key};{record.Value};";
            }

            return res;
        }
    }
}