﻿using Id3v2.Domain;
using Id3v2.Services.Implementations;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Id3v2.Application
{
    public interface IApplication
    {
        string FileName { get; set; }
        string Extension { get; set; }
        char Delimeter { get; set; }

        /// <summary>
        /// Creates decision tree.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Tree CreateDecisionTree(DataTable data);

        /// <summary>
        /// Imports training data for tree from file.
        /// </summary>
        /// <returns></returns>
        DataTable RunImportTrainingData();

        /// <summary>
        /// Imports and prepares test data.
        /// </summary>
        /// <returns></returns>
        List<Dictionary<string, string>> RunImportTestData();

        /// <summary>
        /// Calculates result based on learnt tree and test data.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="testData"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        List<string> CalculateResult(Tree tree, List<Dictionary<string, string>> testData, string result);
    }

    public class Application : IApplication
    {
        private readonly IFileHandler _fileHandler;
        private readonly ITreeCreator _treeCreator;
        private readonly ITreeCalculator _treeCalculator;
        private readonly ITestDataProcessor _testDataProcessor;

        public string FileName { get; set; }
        public string Extension { get; set; }
        public char Delimeter { get; set; }

        public Application(
            IFileHandler fileHandler,
            ITreeCreator treeCreator,
            ITreeCalculator treeCalculator,
            ITestDataProcessor testDataProcessor)
        {
            _fileHandler = fileHandler;
            _treeCreator = treeCreator;
            _treeCalculator = treeCalculator;
            _testDataProcessor = testDataProcessor;
        }

        /// <summary>
        /// Imports training data for tree from file.
        /// </summary>
        /// <returns></returns>
        public DataTable RunImportTrainingData()
        {
            return _fileHandler.ImportTrainingData($"{FileName}.{Extension}", Delimeter);
        }

        /// <summary>
        /// Imports and prepares test data.
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> RunImportTestData()
        {
            DataTable testDataTable = _fileHandler.ImportTestData($"{FileName}-test.{Extension}", Delimeter);
            return _testDataProcessor.ProcessQueriesFromDataTable(testDataTable);
        }

        /// <summary>
        /// Creates decision tree.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Tree CreateDecisionTree(DataTable data)
        {
            Tree tree = new Tree();
            TreeNode root = _treeCreator.Learn(data, "");
            tree.SetRoot(root);
            return tree;
        }

        /// <summary>
        /// Calculates result based on learnt tree and test data.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="testData"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public List<string> CalculateResult(Tree tree, List<Dictionary<string, string>> testData, string result)
        {
            var results = new List<string>();
            foreach (Dictionary<string, string> single in testData)
            {
                string singleResult = _treeCalculator.CalculateQueryResult(tree.Root, single, result);
                results.Add(singleResult);
            }

            return results;
        }
    }
}