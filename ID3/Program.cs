﻿using Autofac;
using ID3.Services.Implementations;
using ID3.Services.Measurements;
using ID3Comparer.Accord;
using Id3v2.Application;
using Id3v2.Domain;
using Id3v2.IoC;
using Id3v2.Services.Measurements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using static System.Console;

namespace Id3v2
{
    /// <summary>
    /// Main class with start-up
    /// </summary>
    class Program
    {
        private static readonly Dictionary<string, TimeSpan> _measurements = new Dictionary<string, TimeSpan>();

        /// <summary>
        /// Starts whole application.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            if (args.Length == 5)
            {
                SetNumberOfWorkingProcessors(args[4]);
            }

            IContainer container = RegisterAll();
            using (ILifetimeScope scope = container.BeginLifetimeScope())
            {
                IApplication app = scope.Resolve<IApplication>();
                GetValuesFromArgs(args, app);
                IMeasurementHandler handler = scope.Resolve<IMeasurementHandler>();

                TimeSpan accordTime = handler.Measure(WithAccordHandler(app, scope.Resolve<IAccordHandler>()));
                TimeSpan baseTime = handler.Measure(WithoutParalellism(app));

                WriteLine("\n");
                string keyBase = $"base-{args[0]}";
                string keyAccord = $"accord-{args[0]}";
                if (args.Length == 5)
                {
                    keyBase = new string(keyBase.Concat($"-mapping_number-{args[4]}").ToArray());
                    keyAccord = new string(keyAccord.Concat($"-mapping_number-{args[4]}").ToArray());
                }

                _measurements[keyBase] = baseTime;
                _measurements[keyAccord] = accordTime;

                IResultPrinter printer = scope.Resolve<IResultPrinter>();
                printer.PrintSpeedup(baseTime, _measurements);

                ICalculationWriter writer = scope.Resolve<ICalculationWriter>();
                await writer.Save(_measurements);
            }
        }

        private static void SetNumberOfWorkingProcessors(string numberOfProcessors)
        {
            var proc = Process.GetCurrentProcess();
            long affinityMask = (long)proc.ProcessorAffinity;
            var parsedNumberOfProcessors = long.Parse(numberOfProcessors);
            affinityMask &= parsedNumberOfProcessors; // use only one of the first 4 available processors

            proc.ProcessorAffinity = (IntPtr)affinityMask;
        }

        private static Action WithAccordHandler(IApplication app, IAccordHandler accordHandler)
        {
            return () =>
            {
                DataTable trainingData = app.RunImportTrainingData();
                List<Dictionary<string, string>> testData = app.RunImportTestData();

                if (trainingData == null)
                {
                    MessageHandler.DisplayErrorMessage($"Data must not be null: {nameof(trainingData)}");
                }

                if (testData == null)
                {
                    MessageHandler.DisplayErrorMessage($"Data must not be null: {nameof(testData)}");
                }

                List<string> results = accordHandler.Handle(trainingData, testData);

                foreach (var result in results)
                {
                    WriteLine(result);
                }
            };
        }

        private static Action WithoutParalellism(IApplication app)
        {
            return () =>
            {
                DataTable trainingData = app.RunImportTrainingData();
                List<Dictionary<string, string>> testData = app.RunImportTestData();

                if (trainingData == null)
                {
                    MessageHandler.DisplayErrorMessage($"Data must not be null: {nameof(trainingData)}");
                }

                if (testData == null)
                {
                    MessageHandler.DisplayErrorMessage($"Data must not be null: {nameof(testData)}");
                }

                Tree tree = app.CreateDecisionTree(trainingData);

                List<string> results = app.CalculateResult(tree, testData, "");
                foreach (var result in results)
                {
                    WriteLine(result);
                }
            };
        }

        private static void GetValuesFromArgs(string[] args, IApplication app)
        {
            app.FileName = args[1];
            app.Extension = args[2];
            app.Delimeter = args[3].First();
        }

        /// <summary>
        /// Registers all dependencies in container.
        /// </summary>
        /// <returns></returns>
        private static IContainer RegisterAll()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application.Application>().As<IApplication>();
            builder.RegisterModule(new ContainerModule());
            return builder.Build();
        }
    }
}